# Fotocompuerta LabFD
![Fotocompuerta](https://gitlab.com/nanocastro/Fotocompuerta-LabFD/raw/master/Imagenes/fotogate%20cerrada.jpg)

Esta es una fotocompuerta simple y de bajo costo para experimentos de física. En este repositorio se encuentran las guías para movimiento armónico pero también puede ser utilizada junto al [riel de aire](https://github.com/pcremades/Riel-de-Aire) para experimentos de cinemática, dinámica y conservación de la energía. 

Está basada en un Led emisor y un receptor IR (https://www.openhacks.com/page/productos/id/599/title/Infrared-Emitters-and-Detectors#.WtCt74jwbIU)

Basado en la DIY Photogate Timer de Sparkfun (https://learn.sparkfun.com/CSC_Sparkfun_001?_ga=2.242122754.16001112.1495028461-1277602422.1495027802)
